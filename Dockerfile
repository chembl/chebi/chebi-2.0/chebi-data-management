FROM python:3.12-slim-bookworm
ENV PYTHONUNBUFFERED 1
ARG PG_HOST
ARG PG_PORT
ARG PG_USER
ARG PG_PASSWORD
ARG PG_DBNAME
ARG DBT_ORIGIN_SCHEMA

ENV PYTHONUNBUFFERED 1
ENV PG_PORT $PG_PORT
ENV PG_USER $PG_USER
# PGPASSWORD env variable needs to mantain that special name. Take a look this stack overflow answer: https://stackoverflow.com/a/8123178/4508767
ENV PGPASSWORD $PG_PASSWORD
ENV PG_DBNAME $PG_DBNAME
ENV PG_HOST $PG_HOST
ENV DBT_ORIGIN_SCHEMA $DBT_ORIGIN_SCHEMA

WORKDIR /chebi_data_management

COPY . /chebi_data_management

RUN pip install -r requirements.txt && \
    dbt deps

CMD ["dbt", "--help"]