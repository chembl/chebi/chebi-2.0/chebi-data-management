-- This macro filters data showed in the ChEBI Data products: SQL DUMPS, Ontology, TSV Files
-- It receives two table aliases: compound_table alias which is an alias for the compounds table
-- And table_name alias which is an alias for a table you want to filter (structure, chemical_data, etc)

{% macro dumps_data_condition(table_name_alias, compounds_table_alias='c') -%}
    ({{- compounds_table_alias -}}.stars = 1 AND {{ table_name_alias -}}.status_id = 3)
    OR ({{- compounds_table_alias -}}.stars = 2 AND {{ table_name_alias -}}.status_id IN (3, 9))
    OR ({{- compounds_table_alias -}}.stars = 3 AND {{ table_name_alias -}}.status_id = 1)
{%- endmacro %}