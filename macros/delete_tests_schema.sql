-- This macro delete schemas with "test_" prefix. It is useful to delete
-- the schemas generated in the data testing process. It is executed once
-- dbt execution has finished. See: https://docs.getdbt.com/reference/project-configs/on-run-start-on-run-end
{% macro delete_tests_schema(schemas) %}
  {% for schema in schemas %}
    {% if schema.startswith('test_') %}
        DROP SCHEMA IF EXISTS {{ schema }} CASCADE;
    {% endif %}
  {% endfor %}
{% endmacro %}
