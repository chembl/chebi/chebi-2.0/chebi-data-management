{% test not_include_secondary_ids(model, column_name, compounds_table) %}
WITH secondary_ids AS (
    SELECT
        c.id AS secondary_id
    FROM {{ compounds_table }} AS c
    WHERE c.parent_id IS NOT NULL
),
validation AS (
    SELECT
        COUNT(*) AS cnt
    FROM {{ model }} AS m
        INNER JOIN secondary_ids AS sids ON sids.secondary_id = m.{{ column_name }}
)
SELECT * FROM validation WHERE cnt > 0
{% endtest %}