{% docs __overview__ %}

# Welcome to the ChEBI database documentation 📚

## Introduction

This is the official ChEBI Database documentation, here you will find explanations, comments and descriptions for all
ChEBI Database tables and columns. Click on `chebi_dumps > models` to see the available documentation. Every subfolder
in `models` folder has a complete description of the columns and data types.

----------

**🚧 SITE UNDER CONSTRUCTION: Please note that some parts of the documentation may change during ChEBI's
redevelopment phase. For example, some columns could be added/deleted or re-named.**

-----------

## About **chebi_allstar** tables

These are the tables included in the [SQL Dumps](https://ftp.ebi.ac.uk/pub/databases/chebi-2/generic_dumps/), and it is the 
source of truth used by the other ChEBI Data Products: [ChEBI Ontology](https://ftp.ebi.ac.uk/pub/databases/chebi-2/ontology/) (all its variants), [ChEBI Flat Files](https://ftp.ebi.ac.uk/pub/databases/chebi-2/flat_files/), 
ChEBI Backend (Rest APIS) and [SDF files](https://ftp.ebi.ac.uk/pub/databases/chebi-2/SDF/). You can access the documentation by
clicking on `chebi_dumps > models > chebi_allstar`.

### Secondary ChEBI IDs 2️⃣
It is important to mark that an entry in ChEBI can have multiple secondary ids and only
one primary id, as well known as the ChEBI ID (our stable identifiers offer during years 👵). For example, the compound [caffeine](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:27732)
has the ChEBI ID (primary id) `CHEBI:27732` and three secondary ids: `CHEBI:22982`, `CHEBI:3295` and `CHEBI:41472`, 
**those reference the same compound**, but the official ChEBI ID for `caffeine` is `CHEBI:27732`. The above is crucial
because a compound could get information (synonyms, cross-references, chemical_data, structure, etc.) from both, 
primary id (ChEBI ID) and the secondary ones. 

It is specially worth mentioning the consequences of having the secondary ids in [chemical_data](#!/model/model.chebi_dumps.chemical_data) and [structure](#!/model/model.chebi_dumps.structure) tables. 
The default compound structure can be related to a secondary id rather than the primary one, we use this
structure to calculate the chemical data (such as charge, mass, and monoisotopic mass) and structural data (like InChI, SMILES, etc.), 
not matter if the default structure refers to a secondary id or primary id. Due to the above, you will find, for example, 
a row in the `structures` and `chemical_data` table that refers to a secondary id in some cases. For example, the compound
[1-alkyl-2-acylglycerol](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:598) has as a primary id `CHEBI:598`, and `CHEBI:19009` as a secondary id.
Using the `chebi_allstar` models and a bit of SQL:

```sql
SELECT * FROM chebi_allstar.structures s WHERE s.compound_id IN (598, 19009);
```
Returns:

| id      | compound_id | status_id | molfile    | smiles                 | standard_inchi  | standard_inchi_key | dimension | default_structure |
|---------|-------------|-----------|------------|------------------------|-----------------|--------------------|-----------|-------------------|
| 2832395 | 19009       | 1         | molfile :) | \*C(=O)OC(CO)CO\[1\*\] |                 |                    | 2D        | true              |


As you can see, the `compound_id` is `19009`, which is the secondary one, we can say that the secondary ID has assigned 
the default structure (column `default_structure` as `True`). Now, take a look at the `chemical_data` table:

```sql
SELECT * FROM chebi_allstar.chemical_data cd WHERE cd.compound_id IN (598, 19009);
```

Returns:

| id    | compound_id | structure_id | status_id | formula   | charge | mass     | monoisotopic_mass |
|-------|-------------|--------------|-----------|-----------|--------|----------|-------------------|
| 15337 | 19009       | 2832395      | 1         | C4H6O4R2  | 0      | 118.088  |                   |
| 610   | 598         |              | 1         |           |        |          | 118.02661         |

In this case, we get two records, which one is the correct?. It will be the first record with the secondary ID (`19009`), 
because it has the default structure (as we saw above).

> 📝 Compound information in ChEBI can come from secondary ids.

> 📝 Secondary ids are scattered in **all chebi_allstar schema**.

## Working with secondary IDs is tricky 😕, are there other options?

Yes, if you want to avoid working with secondary ids records, then you'll like to take a look at the [ChEBI flat files](https://ftp.ebi.ac.uk/pub/databases/chebi-2/flat_files/). 
Please continue reading.

## About chebi_flat_files tables

If you don't want to worry about secondary ChEBI IDs, then we highly recommend using the [ChEBI flat files](https://ftp.ebi.ac.uk/pub/databases/chebi-2/flat_files/).
They include all the **chebi_allstar** tables exported as TSV files (tab separated values) with a big difference. They do **NOT** include secondary ids, so you can be sure
every compound identifier in all the tables are **ALWAYS** the primary one (the ChEBI ID). This data product could be more suitable for your analysis, depending on your goals.

Columns included in the ChEBI flat files have the same meaning as columns included in the `chebi_allstar` schema, therefore you should go to `chebi_dumps > models > chebi_allstar`
to read the documentation. Additionally, we have decided to include an extra TSV file called [secondary_ids](/#!/model/model.chebi_dumps.secondary_ids). It can be useful if you have ChEBI secondary ids,
and you want to get the primary one, documentation of this flat file can be found in `chebi_dumps > models > chebi_flat_files`.

## chebi_allstar ER Diagram

![schema](./assets/schema.png)


_This website was auto-generated using [DBT](https://docs.getdbt.com)_

{% enddocs %}

