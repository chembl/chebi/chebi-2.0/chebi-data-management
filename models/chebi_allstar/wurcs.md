# Model-level Descriptions
{% docs wurcs_table %}

This table stores the [WURCS](https://pubs.acs.org/doi/10.1021/ci400571e) representation for all compounds that are **carbohydrates** ([CHEBI:16646](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:16646)).

{% enddocs %}

# Column-level Descriptions

{% docs wurcs %}

[WURCS](https://pubs.acs.org/doi/10.1021/ci400571e) representation got using the structure referenced by the `structure_id` column.

{% enddocs %}
