# Model-level Descriptions

{% docs comments %}
This table stores compound's comments added by curators and submitters. It could be a general comment or a specific comment
about a specific data item found in any table of the schema.
Based on the compound star number (see the [Compounds](#!/model/model.chebi_dumps.compounds) table), the associated comments will have a status as follows:

| Compound `star`     | Comments `status_id`                                                                                                                     |
|---------------------|------------------------------------------------------------------------------------------------------------------------------------------|
| 3 Star (⭐⭐⭐)        | A 3-star compound includes **ONLY** fully annotated comments (**status_id=1**)                                                           |
| 2 Star (⭐⭐)         | A 2-star compound includes **ONLY** comments annotated by a submitter or a third-party (**status_id=9** or **status_id=3** respectively) |
| 1 Star (⭐)          | A 1-star compound includes **ONLY** chemical data annotated by a third-part, not submitter. (**status_id=3**)                            |

{% enddocs %}

# Column-level Descriptions

{% docs comment %}

comment added by the curator ot submitter

{% enddocs %}

{% docs author_name %}

Username of the person who left the comment

{% enddocs %}

{% docs common_work_in_progress %}
WORK IN PROGRESS ✍️
{% enddocs %}