# Model-level Descriptions

{% docs compound_origins %}
This table stores all the species related information and their sources. For example, caffeine ([CHEBI:27732](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:27732)) can be found in the species 
[Homo Sapiens](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=txid9606). Sometimes, a compound can be found in a specific part (we call it **component**) from a specie; continuing with the caffeine example,
this compound can be found in Homo sapiens, particularly in the component [blood serum](https://purl.obolibrary.org/obo/BTO_0000133).

Based on the compound star number (see the [Compounds](#!/model/model.chebi_dumps.compounds) table), the associated compound origins records 
will have a status as follows:

| Compound `star`     | Compound Origins `status_id`                                                                                                                                 |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 3 Star (⭐⭐⭐)        | A 3-star compound includes **ONLY** fully annotated compound origins data (**status_id=1**)                                                                  |
| 2 Star (⭐⭐)         | A 2-star compound includes **ONLY** compound origins information annotated by a submitter or a third-party (**status_id=9** or **status_id=3** respectively) |
| 1 Star (⭐)          | A 1-star compound includes **ONLY** compound origins information annotated by a third-part, not submitter. (**status_id=3**)                                 |
{% enddocs %}


# Column-level Descriptions

{% docs species_source_id %}
Using the **caffeine** example mentioned above. This column stores the `source_id` where species (i.e. **Homo sapiens**) 
comes from: [NCBI database](https://www.ncbi.nlm.nih.gov)
{% enddocs %}

{% docs species_accession %}
Using the **caffeine** example mentioned above, this column will store the species accession identifier used by 
[NCBI database](https://www.ncbi.nlm.nih.gov) to get the **Homo sapiens** information: [txid9606](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=txid9606)
{% enddocs %}

{% docs species_text %}
Using the **caffeine** example mentioned above, this column will store the text **Homo sapiens**.
{% enddocs %}

{% docs component_source_id %}
Using the **caffeine** example mentioned above, this column will store the `source_id` where **blood serum** information comes from: [The BRENDA Tissue Ontology (BTO)](https://ontobee.org/ontology/BTO)
{% enddocs %}

{% docs component_accession %}
Using the **caffeine** example mentioned above, this column will store the component accession identifier used by 
[The BRENDA Tissue Ontology (BTO)](https://ontobee.org/ontology/BTO) to get the **blood serum** information: [BTO_0000133](http://purl.obolibrary.org/obo/BTO_0000133)
{% enddocs %}

{% docs component_text %}
Using the **caffeine** example mentioned above, this column will store the text **blood serum**.
{% enddocs %}

{% docs compound_origins_comments %}
Extra comments added by the ChEBI curators.
{% enddocs %}

{% docs compound_origins_source_id %}
This column stores the `source_id` that claims _"caffeine can be found in Homo sapiens, particularly in the component blood serum."_: [MetaboLights](https://www.ebi.ac.uk/metabolights)
{% enddocs %}

{% docs compound_origins_source_accession %}
This column stores the resource identifier that claims _"caffeine can be found in Homo sapiens, particularly in the component blood serum."_: [MTBLS90](https://www.ebi.ac.uk/metabolights/editor/MTBLS90/descriptors)
{% enddocs %}

{% docs strain_text %}
A strain is a genetic variant, a subtype or a culture within a biological species (See [here](https://en.wikipedia.org/wiki/Strain_(biology))). 
If the compound can be found in a strain, then this column will store that information.
{% enddocs %}