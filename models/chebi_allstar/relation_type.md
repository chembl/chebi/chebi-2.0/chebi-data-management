# Model-level Descriptions

{% docs relation_type %}
This table stores the definition of the possible ontological relations among entities
{% enddocs %}

# Column-level Descriptions

{% docs relation_type_identifier %}
Unique integer identifier for the relation data record
{% enddocs %}

{% docs relation_type_code %}
Official unique name of the relation in ChEBI, possible values are:
* has_functional_parent
* has_parent_hydride
* has_part
* has_role
* is_a
* is_conjugate_acid_of
* is_conjugate_base_of
* is_enantiomer_of
* is_part_of
* is_substituent_group_from
* is_tautomer_of
{% enddocs %}

{% docs allow_cycles %}
Boolean column to identify if a relation allows cycles: **TRUE** if the relation allows cycle. **FALSE** the relation does not allow cycles.
{% enddocs %}

{% docs relation_type_description %}
Short description about the relation.
{% enddocs %}