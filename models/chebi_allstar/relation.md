# Model-level Descriptions

{% docs relation %}

This table stores the relation between two entities. Each row in this table
can be represented as a DAG (Direct acyclic graph)

![relation_graph.png](./assets/relation_graph.png)

[//]: # (```mermaid)

[//]: # ()
[//]: # ()
[//]: # (graph LR)

[//]: # ()
[//]: # ()
[//]: # (    A&#40;"CHEBI:init_id )

[//]: # ()
[//]: # (    Any star&#40;⭐, ⭐⭐, ⭐⭐⭐&#41;"&#41; -- relation_type_id)

[//]: # ()
[//]: # (    )
[//]: # (    Status&#40;1, 3, 9&#41;)

[//]: # ()
[//]: # (    --> B&#40;"CHEBI:final_id)

[//]: # ()
[//]: # (    Any star&#40;⭐, ⭐⭐, ⭐⭐⭐&#41;"&#41;)

[//]: # ()
[//]: # ()
[//]: # (```)

Relation arch goes from `init_id` to `final_id`, both ids are FK to the [compounds](#!/model/model.chebi_dumps.compounds) table.
Also, `relation_type_id` is a FK to the [relation_type](#!/model/model.chebi_dumps.relation_type) table.

This table only includes relations with a valid `status_id` (See [status](#!/model/model.chebi_dumps.status) table, column `status_id`), not mater the number of stars
an entity has. So, for example, these kinds of relations are possible:

### 1-star initial entity and 3-star final entity

![relation_graph.png](./assets/relation_1_3.png)

### 2-star initial entity and 1-star final entity

![relation_graph.png](./assets/relation_2_1.png)
### 3-star initial entity and 2-star final entity

![relation_graph.png](./assets/relation_3_2.png)

And whatever star rating for both, initial and final entity.



[//]: # (```mermaid)

[//]: # (graph LR)

[//]: # (    A&#40;"CHEBI:init_id &#40;⭐⭐⭐&#41;"&#41; -- relation_type_id )

[//]: # (    )
[//]: # (    Status&#40;1, 3, 9&#41; --> B&#40;"CHEBI:final &#40;⭐⭐⭐&#41;"&#41;)

[//]: # (    )
[//]: # (    C&#40;"CHEBI:init_id &#40;⭐&#41;"&#41; -- relation_type_id )

[//]: # (    )
[//]: # (    Status&#40;1, 3, 9&#41; --> D&#40;"CHEBI:final &#40;⭐&#41;"&#41;)

[//]: # (    )
[//]: # (    E&#40;"CHEBI:init_id &#40;⭐⭐&#41;"&#41; -- relation_type_id )

[//]: # (    )
[//]: # (    Status&#40;1, 3, 9&#41; --> F&#40;"CHEBI:final &#40;⭐⭐&#41;"&#41;)

[//]: # (    )
[//]: # (    G&#40;"CHEBI:init_id &#40;⭐&#41;"&#41; -- relation_type_id )

[//]: # (    )
[//]: # (    Status&#40;1, 3, 9&#41; --> H&#40;"CHEBI:final &#40;⭐⭐⭐&#41;"&#41;)

[//]: # (    )
[//]: # (    I&#40;"CHEBI:init_id &#40;⭐⭐⭐&#41;"&#41; -- relation_type_id )

[//]: # (    )
[//]: # (    Status&#40;1, 3, 9&#41; --> J&#40;"CHEBI:final &#40;⭐⭐&#41;"&#41;)

[//]: # (    )
[//]: # (    K&#40;"CHEBI:init_id &#40;⭐⭐&#41;"&#41; -- relation_type_id )

[//]: # (    )
[//]: # (    Status&#40;1, 3, 9&#41; --> L&#40;"CHEBI:final &#40;⭐&#41;"&#41;)

[//]: # (```)

{% enddocs %}

# Column-level Descriptions

{% docs relation_type_id %}

Foreign key to the [relation_type](#!/model/model.chebi_dumps.relation_type) table.

{% enddocs %}

{% docs evidence_accession %}

A resource identifier to evidence the veracity of the relation, provided by external databases.

{% enddocs %}