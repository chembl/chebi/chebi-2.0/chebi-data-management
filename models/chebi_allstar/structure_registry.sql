WITH structure_registry AS (
    SELECT
        sr.id,
        sr.structure_id,
        sr.layers,
        sr.hash
    FROM {{ source('chebi_production', 'structure_registry') }} AS sr
    INNER JOIN {{ ref('structures') }} AS s ON sr.structure_id = s.id
)

SELECT * FROM structure_registry
