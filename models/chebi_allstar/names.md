# Model-level Descriptions

{% docs names %}
This table stores all the information related to the Compounds names (IUPAC names, exact synonyms and related synonyms). 
Based on the compound star number (see the [Compounds](#!/model/model.chebi_dumps.compounds) table), the associated names records will have a status as follows:

| Compound `star`     | Names `status_id`                                                                                                                     |
|---------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| 3 Star (⭐⭐⭐)        | A 3-star compound includes **ONLY** fully annotated names (**status_id=1**)                                                           |
| 2 Star (⭐⭐)         | A 2-star compound includes **ONLY** names annotated by a submitter or a third-party (**status_id=9** or **status_id=3** respectively) |
| 1 Star (⭐)          | A 1-star compound includes **ONLY** names annotated by a third-part, not submitter. (**status_id=3**)                                 |
{% enddocs %}

# Column-level Descriptions

{% docs language_code %}
Code of the language used by the name. Values can be written in English 🇬🇧 (en), Spanish 🇪🇸 (es), French 🇫🇷 (fr), 
German 🇩🇪 (de) and Latin (la).
{% enddocs %}

{% docs name %}
Compound name. Notice that this is different to the ChEBI Name, which is **ALWAYS** in the [Compounds](#!/model/model.chebi_dumps.compounds) table.
{% enddocs %}

{% docs common_name %}
It may contain special characters such as α, β as well as standard HTML tags for powers, subscripts and superscripts.
For example: 
- name of [CHEBI:33813](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:33813) is `(<small><sup>18</small></sup>O)water`
- name of [CHEBI:30216](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:30216) is  `α-particle`
- name of [CHEBI:39245](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:39245) is  `β,β-trehalose`
{% enddocs %}

{% docs ascii_name %}
ASCII compound name. Notice that this is different to the ChEBI ASCII Name, which is **ALWAYS** in the [Compounds](#!/model/model.chebi_dumps.compounds) table.
{% enddocs %}

{% docs common_ascii_name %}
This table shows some examples of the `name` and `ascii_name` values.

| `name` column                           | `ascii_name` column |
|-----------------------------------------|---------------------|
| (<small><sup>18</small></sup>O)water    | ((18)O)water        |
| α-particle                              | alpha-particle      |
| β,β-trehalose                           | beta,beta-trehalose |
{% enddocs %}

{% docs name_type %}
Type of the name. Possible values are: `INN`, `BRAND`, `IUPAC_NAME`, `SYNONYM` and `NAME`. They mean:

- **INN:** In cases where an entity is a pharmaceutical substance, an International Nonproprietary Name ([INN](https://www.who.int/teams/health-product-and-policy-standards/inn/)) may be provided.
- **BRAND NAME:** Where an entity is an active ingredient of a proprietary pharmaceutical preparation, the brand name of the preparation may be shown. For instance Panadol and Tylenol are brand names for paracetamol
- **IUPAC_NAME:** A name provided for an entity based on current recommendations of [IUPAC](https://iupac.org).
- **SYNONYM:** Alternative names for an entity which either have been used in EBI or external sources or have been devised by the curators based on recommendations of IUPAC, NC-IUBMB or their associated bodies.
- **UNIPROT NAME:** A name provided for an entity based on current recommendations of [UniProt](https://www.uniprot.org)
{% enddocs %}

{% docs adapted %}
Normally names are reproduced in the exact form they appear in the source; however, sometimes 
curators have to perform changes (e.g. fix syntax errors) on the name. This column will be **TRUE** if any kind of editing was performed, otherwise it will be **FALSE**.
{% enddocs %}