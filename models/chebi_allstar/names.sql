WITH names AS (
    SELECT
        n.id,
        n.compound_id,
        n.name,
        n.type,
        n.status_id,
        n.source_id,
        n.adapted,
        n.language_code,
        n.ascii_name
    FROM {{ source('chebi_production', 'names') }} AS n
    INNER JOIN (SELECT id, stars FROM {{ ref('compounds') }}) AS c
        ON n.compound_id = c.id
    WHERE
        {{ dumps_data_condition('n') }}
)

SELECT
    n.*
FROM names AS n
LEFT JOIN {{ ref('source') }} AS so ON n.source_id = so.id
