WITH comments AS (
    SELECT
        co.id,
        co.compound_id,
        co.comment,
        co.author_name,
        co.status_id,
        co.datatype,
        co.datatype_id
    FROM {{ source('chebi_production', 'comments') }} AS co
    INNER JOIN (SELECT id, stars FROM {{ ref('compounds') }}) AS c
        ON co.compound_id = c.id
    WHERE
        {{ dumps_data_condition('co') }}
)

SELECT * FROM comments
