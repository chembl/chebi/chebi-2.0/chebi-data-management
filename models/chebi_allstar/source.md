# Model-level Descriptions

{% docs source %}
Source table stores information related to the different biological databases 
(e.g. [KEGG COMPOUND](https://www.genome.jp/kegg/compound/), [Drug Bank](https://go.drugbank.com/drugs)) and biological ontologies (e.g. [UBERON](https://obophenotype.github.io/uberon/), [GO](https://geneontology.org)) where the data comes from.

{% enddocs %}

# Column-level Descriptions

{% docs common_source_id %}
Foreign key to [Source](#!/model/model.chebi_dumps.source) table.
{% enddocs %}

{% docs source_name %}
Source's name, for example: UBERON, Rhea or KEGG COMPOUND 
{% enddocs %}

{% docs source_url %}
Source's URL. It is used the standard URLs from [Bioregistry repository](https://bioregistry.io) if possible.
{% enddocs %}

{% docs source_description %}
Short description of the source
{% enddocs %}

{% docs source_prefix %}
Source's prefix got from [Bioregistry repository](https://bioregistry.io). These prefixes (officially, the first part of a [**curie**](https://cthoyt.com/2021/09/14/curies.html)) 
are used in the [ChEBI Ontology Generation Process](https://gitlab.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-ontology-generator) to create 
the different cross-references.
{% enddocs %}