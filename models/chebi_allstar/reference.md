# Model-level Descriptions
{% docs reference %}

This table stores all the cross-references generated **automatically** by third-parties.

{% enddocs %}

# Column-level Descriptions

{% docs common_id %}

Unique identifier for the record.

{% enddocs %}

{% docs reference_accession_number %}
Resource identifier used by the database that references the compound.
{% enddocs %}

{% docs reference_name %}
Compound name given by the other databases.
{% enddocs %}

