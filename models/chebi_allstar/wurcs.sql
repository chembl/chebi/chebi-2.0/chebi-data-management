WITH wurcs AS (
    SELECT
        w.structure_id,
        w.wurcs
    FROM {{ source('chebi_production', 'wurcs') }} AS w
        INNER JOIN
            (SELECT id FROM {{ ref('structures') }}) AS s
            ON w.structure_id = s.id
)

SELECT * FROM wurcs
