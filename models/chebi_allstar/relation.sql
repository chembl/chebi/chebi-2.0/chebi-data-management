WITH relation AS (
    SELECT DISTINCT
        r.id,
        r.relation_type_id,
        r.init_id,
        r.final_id,
        r.status_id,
        r.evidence_accession,
        r.evidence_source_id
    FROM {{ source('chebi_production', 'relation') }} AS r
    INNER JOIN {{ source('chebi_production', 'relation_type') }} AS rt ON rt.id = r.relation_type_id
    INNER JOIN (SELECT comp.id, comp.stars FROM {{ ref('compounds') }} AS comp) AS c ON r.init_id = c.id
    INNER JOIN (SELECT comp.id, comp.stars FROM {{ ref('compounds') }} AS comp) AS c2 ON r.final_id = c2.id
    WHERE
        r.status_id IN (SELECT s.id FROM {{ ref('status') }} AS s)
)

SELECT * FROM relation
