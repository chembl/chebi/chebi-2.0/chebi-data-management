-- Here we need to create a post-hook because compounds table is an special one, it has FK to itself.
-- So we are assuring we are creating FK in the parent_id column
{{ config(
    post_hook="ALTER TABLE {{ this.schema }}.compounds ADD CONSTRAINT compound_id__to__parent_id FOREIGN KEY (parent_id) REFERENCES {{ this.schema }}.compounds(id) ON DELETE SET NULL;"
) }}

{% set dumps_valid_stars = (1, 2, 3) %}

WITH cte_compounds AS (
    SELECT DISTINCT
        c.id,
        c.name,
        c.status_id,
        c.source,
        c.parent_id,
        c.merge_type,
        c.chebi_accession,
        c.definition,
        c.ascii_name,
        c.stars,
        c.modified_on,
        c.release_date
    FROM {{ source('chebi_production', 'compounds') }} AS c
    INNER JOIN {{ source('chebi_production', 'status') }} AS st ON c.status_id = st.id
    LEFT JOIN (
        SELECT
            comps.parent_id,
            comps.stars,
            comps.status_id
        FROM {{ source('chebi_production', 'compounds') }} AS comps
    ) AS c2 ON c.id = c2.parent_id
    WHERE
        c.stars IN {{ dumps_valid_stars }}
),

-- Should we update 1-star parents to 2-star ?
cte_parents_compounds AS (
    SELECT c.* FROM cte_compounds AS c
    WHERE c.stars IN {{ dumps_valid_stars }} AND c.parent_id IS NULL
),

-- Get all children from 1-star, 2-star and 3-star parents
cte_children_compounds AS (
    SELECT c2.*
    FROM cte_parents_compounds AS c
    INNER JOIN cte_compounds AS c2 ON c.id = c2.parent_id
),

cte_valid_compounds AS (
    SELECT * FROM cte_parents_compounds
    UNION
    SELECT * FROM cte_children_compounds
)

SELECT cvc.*
FROM cte_valid_compounds AS cvc
