WITH database_accession AS (
    SELECT
        da.id,
        da.compound_id,
        da.accession_number,
        da.type,
        da.status_id,
        da.source_id
    FROM {{ source('chebi_production', 'database_accession') }} AS da
    INNER JOIN (SELECT id, stars FROM {{ ref('compounds') }}) AS c
        ON da.compound_id = c.id
    WHERE
        {{ dumps_data_condition('da') }}
)

SELECT
    da.*
FROM database_accession AS da
LEFT JOIN {{ ref('source') }} AS so ON da.source_id = so.id
