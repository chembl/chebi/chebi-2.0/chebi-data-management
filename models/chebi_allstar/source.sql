SELECT
    so.id,
    so.name,
    so.url,
    so.prefix,
    so.description
FROM {{ source('chebi_production', 'source') }} AS so
