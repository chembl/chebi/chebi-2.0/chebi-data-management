WITH structures AS (
    SELECT
        s.id,
        s.compound_id,
        s.status_id,
        s.molfile,
        s.smiles,
        s.standard_inchi,
        s.standard_inchi_key,
        s.dimension,
        s.default_structure
    FROM {{ source('chebi_production', 'structures') }} AS s
    INNER JOIN (SELECT id, stars FROM {{ ref('compounds') }}) AS c
        ON s.compound_id = c.id
    WHERE
        {{ dumps_data_condition('s') }}
)

SELECT * FROM structures
