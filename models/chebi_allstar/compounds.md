# Model-level Descriptions

{% docs compounds %}
ChEBI compounds table. It includes separated records for secondary ids. Notice that this table stores ChEBI ontological classes as well,
those are not proper compounds, but concepts (these do not have chemical data or structural data). For example: 

- mutagen ([CHEBI:25435](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:25435))
- food additive ([CHEBI:64047](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:64047))
- environmental contaminant ([CHEBI:78298]((https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:78298))

In any case, we will use the word **entity** to refer a record in the Compounds table whether an ontological class or a proper compound.
{% enddocs %}

# Column-level Descriptions

{% docs compound_id %}
Unique entity identifier. You can be familiarized with these kinds of ChEBI identifiers: `CHEBI:27732` or `CHEBI:15377`.
The above identifiers are known as [curies](https://cthoyt.com/2021/09/14/curies.html). This column
saves the curie's second part after the colon (i.e. 27732, 15377). Take into account ids in this column can be secondary ids.
{% enddocs %}

{% docs compound_name %}
The name for an entity recommended by ChEBI for use by the biological community (a.k.a. ChEBI Name). In general, traditional names have been retained by ChEBI, 
but these may have been modified to enhance clarity, avoid ambiguity and follow more closely current IUPAC recommendations on chemical nomenclature.
{% enddocs %}

{% docs compound_status_id %}
**⚠️ WARNING: This column is deprecated, and it will be maintained for historical reasons only. You SHOULD NOT use this column to filter compounds accordingly
to the level of curation, please use the `star` column.**
{% enddocs %}

{% docs parent_id %}
Foreign key to [Compounds](#!/model/model.chebi_dumps.compounds) table (self referencing). Store the primary id (a.k.a. ChEBI ID) for secondary ids entities. For example, caffeine ([CHEBI:27732](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:27732)) has 3 secondary ids:
[CHEBI:22982](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:22982), [CHEBI:3295](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:3295) and [CHEBI:41472](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:41472), their values
for `parent_id` will be then **27732** (id for caffeine):

| id    | name       | **parent_id**  |
|-------|------------|----------------|
| 22982 | Compound A | **27732**      |
| 3295  | Compound B | **27732**      |
| 41472 | Compound C | **27732**      |

**This column does not mean anything about ontological relations**
{% enddocs %}

{% docs compound_source %}
Informative text about where the entity initially comes from.
{% enddocs %}

{% docs chebi_accession %}
Unique ChEBI identifier as a [curie]((https://cthoyt.com/2021/09/14/curies.html). It **ALWAYS** follows the pattern `CHEBI:NUMBER`, (e.g [CHEBI:27732](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:27732)).
Take a look that this column **ALWAYS** takes the second part of the curie from the column `id`. For example:

| id        | name     | **chebi_accession** |
|-----------|----------|---------------------|
| **27732** | caffeine | CHEBI:**27732**     |
| **15377** | water    | CHEBI:**15377**     |
{% enddocs %}

{% docs definition %}
A short verbal definition for the entity.
{% enddocs %}

{% docs chebi_ascii_name %}
ChEBI name provided as ASCII format.
{% enddocs %}

{% docs release_date %}
Date on which the entity was released to the public domain.
{% enddocs %}

{% docs modified_on %}
Date on which the entity was last updated.
{% enddocs %}


{% docs stars %}
Star number, possible values are: 1, 2, and 3. They mean:

- **3 Star (⭐⭐⭐)**: An entity which has been fully annotated by the ChEBI team.
- **2 Star (⭐⭐)**: An entity which has been submitted to ChEBI but has yet to be annotated by the ChEBI team. The entity could be sent by a submitter using the submission tool, or load into ChEBI from other sources.
- **1 Star (⭐)**: A preliminary entity created by the ChEBI team which is awaiting annotation or loaded.
{% enddocs %}