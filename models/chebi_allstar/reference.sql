WITH reference AS (
    SELECT
        r.id,
        r.compound_id,
        r.location_in_ref,
        r.source_id,
        r.accession_number,
        r.reference_name
    FROM {{ source('chebi_production', 'reference') }} AS r
    INNER JOIN (SELECT id, stars FROM {{ ref('compounds') }}) AS c
        ON r.compound_id = c.id
    WHERE
        c.stars IN (2, 3) -- reference table does not have a status column in Oracle.
)

SELECT
    r.*
FROM reference AS r
LEFT JOIN {{ ref('source') }} AS so ON r.source_id = so.id
