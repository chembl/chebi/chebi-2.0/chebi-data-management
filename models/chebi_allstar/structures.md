# Model-level Descriptions
{% docs structures %}
Structure related information. A compound can have zero or more structures but only one default structure. The last one is
used to calculate structural and chemical data. For example, take a look at the **caffeine** ([CHEBI:27732](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:27732)) compound:

```sql
SELECT * FROM chebi_allstar.structures s WHERE s.compound_id = 27732;
```
You will get:

| id    | compound_id | status_id | default_structure   | molfile     |
|-------|-------------|-----------|---------------------|-------------|
| 3601  | 27732       | 1         | FALSE               | molfile 1   |
| 27934 | 27732       | 1         | FALSE               | molfile 2   |
| 27932 | 27732       | 1         | **TRUE**            | molfile 3   |

So, the last of the above records is the default structure, and we will use it to calculate both 
the structural and chemical data. In the same way, the default compound structure can be related to a secondary id rather than the primary one. [**We have explained
this case here, please take your time to understand it**](#!/overview)

------------------------------

Based on the compound star number (see the [Compounds](#!/model/model.chebi_dumps.compounds) table), the associated structural information will have a status as follows:

| Compound `star`     | Structure `status_id`                                                                                                                           |
|---------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| 3 Star (⭐⭐⭐)        | A 3-star compound includes **ONLY** fully annotated structural data (**status_id=1**)                                                           |
| 2 Star (⭐⭐)         | A 2-star compound includes **ONLY** structural data annotated by a submitter or a third-party (**status_id=9** or **status_id=3** respectively) |
| 1 Star (⭐)          | A 1-star compound includes **ONLY** structural data annotated by a third-part, not submitter. (**status_id=3**)                                 |


{% enddocs %}
# Column-level Descriptions

{% docs inchi %}
InChI identifier for the compound. InChI is a structure-based chemical identifier, developed by [IUPAC](https://iupac.org/) and the [InChI Trust](https://www.inchi-trust.org/). More info [here](https://github.com/IUPAC-InChI/InChI)
{% enddocs %}

{% docs inchi_key %}
The hashed version of the InChI with a fixed length of 27 characters. More info [here](https://github.com/IUPAC-InChI/InChI)
{% enddocs %}

{% docs smiles %}
SMILES (Simplified Molecular Input Line Entry System) identifier for the compound. SMILES is a simple but comprehensive chemical line notation. More info [here](https://www.daylight.com/smiles/)
{% enddocs %}

{% docs default_structure %}
A column to indicate if the record is the compound's default structure. **TRUE** = it is the default structure, **FALSE** = it is NOT the default structure
{% enddocs %}

{% docs molfile %}
MDL molfile for the structure
{% enddocs %}

#TODO: Fix this once we convert 3d structures to 2d structures
{% docs dimension %}
Available structure dimension. Possible values: 2D, 3D.

**ℹ️ INFO: We are working on converting the current 3D structures to 2D structures, ChEBI won't support 3D structures anymore.**
{% enddocs %}