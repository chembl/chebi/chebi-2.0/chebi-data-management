# Model-level Descriptions

{% docs database_accession %}
This table stores all the manually curated cross-references.
For example: 

- **caffeine** ([CHEBI:27732](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:27732)) exists in other databases such as [KEGG DRUG](https://www.kegg.jp/kegg/drug/), and the KEGG DRUG identifier is: [D00528](https://www.kegg.jp/entry/D00528) 
- **caffeine** ([CHEBI:27732](https://wwwdev.ebi.ac.uk/chebi/alpha/CHEBI:27732)) is referenced in [Europe PMC](https://europepmc.org) with the identifier [23551936](https://europepmc.org/article/MED/23551936).

Based on the compound star number (see the [Compounds](#!/model/model.chebi_dumps.compounds) table), the associated database accession records 
will have a status as follows:

| Compound `star`     | Database Accession `status_id`                                                                                                                      |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 3 Star (⭐⭐⭐)        | A 3-star compound includes **ONLY** fully annotated database accessions (**status_id=1**)                                                           |
| 2 Star (⭐⭐)         | A 2-star compound includes **ONLY** database accessions annotated by a submitter or a third-party (**status_id=9** or **status_id=3** respectively) |
| 1 Star (⭐)          | A 1-star compound includes **ONLY** database accessions annotated by a third-part, not submitter. (**status_id=3**)                                 |
{% enddocs %}

# Column-level Descriptions

{% docs accession_number %}
Character column that stores the resource identifier used by the database that is referencing the compound. 
For example, [D00528](https://www.kegg.jp/entry/D00528), [23551936](https://europepmc.org/article/MED/23551936), etc.
{% enddocs %}

{% docs database_accession_type %}
We group the database identifiers in four types. They are:
- **CAS:** Resource identifier is a [CAS Registry Number](https://en.wikipedia.org/wiki/CAS_Registry_Number).
- **REGISTRY_NUMBER:** Resource identifier is a Registry Number, but it does not come from [CAS](https://commonchemistry.cas.org).
- **CITATION:** Resource identifier comes from a literature database such us [Europe PMC](https://europepmc.org).
- **MANUAL_X_REF:** Resource identifier comes from biological databases.

This column stores any of the above types.
{% enddocs %}