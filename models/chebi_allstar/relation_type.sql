WITH cte_relation_type AS (
    SELECT
        rt.id,
        rt.code,
        rt.allow_cycles,
        rt.description
    FROM {{ source('chebi_production', 'relation_type') }} AS rt
)

SELECT * FROM cte_relation_type
