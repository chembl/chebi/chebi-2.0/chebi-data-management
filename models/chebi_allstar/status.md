# Model-level Descriptions

{% docs status_table %}
Store information about statuses. 
This is the way that we have to mark the level of curation in other tables different to the [Compounds](#!/model/model.chebi_dumps.compounds) table, which uses the Star rating.

> 📝 Star gets the level of curation for a Compound, Status gets the level of curation for the rest of the tables.

{% enddocs %}

# Column-level Descriptions

{% docs status_id %}
Possible values are: 1, 3 and 9 and they mean:

- **Status 1 :** (a.k.a. CHECKED). Entity which has been fully annotated by the ChEBI team
- **Status 3 :** (a.k.a. OK). Entity has been annotated **ONLY** by a third-party, and it has not yet been checked by a ChEBI curator (in a lot of these cases, the data has been loaded into ChEBI from a different source)
- **Status 9 :** (a.k.a. SUBMITTED). Entities deposited **ONLY** by a submitter into the ChEBI Database via submission tool.

{% enddocs %}

{% docs common_status_id %}
Foreign key to [Status](#!/model/model.chebi_dumps.status) table.
{% enddocs %}

{% docs status_name %}
Unique status name. Possible values are: `CHECKED`, `OK` and `SUBMITTED`.
{% enddocs %}