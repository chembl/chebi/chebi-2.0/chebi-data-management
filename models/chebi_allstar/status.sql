{% set dumps_valid_status = (1, 3, 9) %}
SELECT
    s.id,
    s.name
FROM {{ source('chebi_production', 'status') }} AS s
WHERE s.id IN {{ dumps_valid_status }}
