WITH compound_origins AS (
    SELECT
        co.id,
        co.compound_id,
        co.species_source_id,
        co.species_text,
        co.species_accession,
        co.component_source_id,
        co.component_text,
        co.component_accession,
        --TODO: Refactor this column, they could be unnecessary
        co.strain_text,
        --co.strain_accession,
        co.source_id,
        co.source_accession,
        co.comments,
        co.status_id
    FROM {{ source('chebi_production', 'compound_origins') }} AS co
    INNER JOIN (SELECT id, stars FROM {{ ref('compounds') }}) AS c
        ON co.compound_id = c.id
    WHERE
        {{ dumps_data_condition('co') }}
)

SELECT
    co.*
FROM compound_origins AS co
LEFT JOIN {{ ref('source') }} AS so ON co.source_id = so.id
