{{ config(alias='reference') }}
WITH cte_references AS (
    SELECT
        r.id,
        r.compound_id,
        r.location_in_ref,
        r.source_id,
        r.accession_number,
        r.reference_name
    FROM {{ ref('reference') }} AS r
    INNER JOIN {{ ref('flat_compounds') }} AS fc ON r.compound_id = fc.id
    ORDER BY r.compound_id
)

SELECT * FROM cte_references
