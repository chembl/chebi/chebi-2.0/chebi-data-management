# Model-level Descriptions

{% docs secondary_ids %}

This table stores the secondary ids related to the primary one. An entry in ChEBI can have multiple secondary ids and **only
one primary id**, as well known as the ChEBI ID (our stable identifiers offer during years 👵). Notice that each row in this table
is a unique tuple of **(primary_id, secondary_id)**. For example, if you have a secondary id for caffeine, let's say `CHEBI:41472`, you
can easily get the primary ChEBI ID as follows:

```python
import pandas as pd

secondary_ids = pd.read_csv('https://ftp.ebi.ac.uk/pub/databases/chebi-2/flat_files/secondary_ids.tsv.gz', index=False, sep='\t')
primary_id = secondary_ids.query("secondary_id == 41472")
print(primary_id)
```

You will get:

| compound_id | secondary_id |
|-------------|--------------|
| 27732       | 41472        |

The above means that **CHEBI:41472** is a secondary id of **CHEBI:27732**. At this point, you can use the primary ChEBI ID to
get more information using the other flat files.

{% enddocs %}


# Column-level Descriptions

{% docs primary_id %}
This column stores the primary id
{% enddocs %}

{% docs secondary_id %}
This column stores the secondary id
{% enddocs %}