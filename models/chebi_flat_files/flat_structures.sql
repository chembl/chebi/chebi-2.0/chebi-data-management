{{ config(alias='structures') }}
WITH cte_structures AS (
    SELECT
        vs.structure_id AS id,
        vs.compound_id,
        s.status_id,
        s.molfile,
        vs.smiles,
        vs.standard_inchi,
        vs.standard_inchi_key,
        s.dimension,
        s.default_structure
    FROM {{ ref('flat_compounds') }} AS fc
    INNER JOIN {{ ref('valid_structures') }} AS vs
        ON fc.id = vs.compound_id
    INNER JOIN {{ ref('structures') }} AS s
        ON vs.structure_id = s.id
)

SELECT * FROM cte_structures
