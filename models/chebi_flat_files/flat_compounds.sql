{{ config(alias='compounds') }}
WITH compounds_without_secondary AS (
    SELECT
        c.id,
        c.name,
        c.status_id,
        c.source,
        c.parent_id,
        c.merge_type,
        c.chebi_accession,
        c.definition,
        c.ascii_name,
        c.stars,
        c.release_date
    FROM {{ ref('compounds') }} AS c
    INNER JOIN {{ ref('compound_base_information') }} AS cbi
        ON c.id = cbi.id
    ORDER BY c.id
)

SELECT * FROM compounds_without_secondary
