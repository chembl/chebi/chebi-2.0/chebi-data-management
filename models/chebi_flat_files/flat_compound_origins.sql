{{ config(alias='compound_origins') }}
WITH cte_compounds_origins AS (
    SELECT
        co.id,
        co.compound_id,
        co.species_source_id,
        co.species_text,
        co.species_accession,
        co.component_source_id,
        co.component_text,
        co.component_accession,
        co.strain_text,
        --TODO: Refactor this column, they could be unnecessary
        --co.strain_accession,
        co.source_id,
        co.source_accession,
        co.comments,
        co.status_id
    FROM {{ ref('valid_compound_origins') }} AS co
    INNER JOIN {{ ref('flat_compounds') }} AS fc ON co.compound_id = fc.id
    ORDER BY co.compound_id
)

SELECT * FROM cte_compounds_origins
