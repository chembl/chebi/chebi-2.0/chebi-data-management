WITH secondary_ids AS (
    SELECT
        vsi.id AS compound_id,
        vsi.secondary_id
    FROM {{ ref('valid_secondary_ids') }} AS vsi
)

SELECT * FROM secondary_ids
