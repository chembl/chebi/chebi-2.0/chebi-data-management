{{ config(alias='database_accession') }}
WITH cte_database_accession AS (
    SELECT
        da.id,
        fc.id AS compound_id,
        da.status_id,
        vdref.source_id,
        vdref.accession_number,
        vdref.type
    FROM {{ ref('flat_compounds') }} AS fc
    INNER JOIN {{ ref('valid_database_references') }} AS vdref
        ON fc.id = vdref.compound_id
    INNER JOIN {{ ref('database_accession') }} AS da
        ON vdref.compound_id = da.compound_id AND vdref.accession_number = da.accession_number
    ORDER BY fc.id
)

SELECT * FROM cte_database_accession
