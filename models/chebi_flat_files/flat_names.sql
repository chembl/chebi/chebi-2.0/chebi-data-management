{{ config(alias='names') }}
WITH exact_synonyms AS (
    SELECT
        aes.compound_id,
        aes.exact_synonym AS name,
        aes.ascii_name
    FROM {{ ref('axioms_exact_synonyms') }} AS aes
),

related_synonyms AS (
    SELECT
        aes.compound_id,
        aes.related_synonym AS name,
        aes.ascii_name
    FROM {{ ref('axioms_related_synonyms') }} AS aes
),

synonyms AS (
    SELECT * FROM exact_synonyms
    UNION
    SELECT * FROM related_synonyms
),

cte_names AS (
    SELECT
        n.id,
        syn.compound_id,
        n.status_id,
        n.source_id,
        syn.name,
        n.type,
        n.adapted,
        n.language_code,
        syn.ascii_name
    FROM {{ ref('flat_compounds') }} AS fc
    INNER JOIN synonyms AS syn
        ON fc.id = syn.compound_id
    INNER JOIN {{ ref('names') }} AS n
        ON syn.compound_id = n.compound_id AND syn.ascii_name = n.ascii_name
    ORDER BY syn.compound_id
)

SELECT * FROM cte_names
