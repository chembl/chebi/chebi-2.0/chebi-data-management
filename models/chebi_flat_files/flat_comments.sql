{{ config(alias='comments') }}
WITH cte_comments AS (
    SELECT
        co.id,
        co.compound_id,
        co.comment,
        co.author_name,
        co.status_id,
        co.datatype,
        co.datatype_id
    FROM {{ ref('comments') }} AS co
    INNER JOIN {{ ref('flat_compounds') }} AS fc ON co.compound_id = fc.id
    ORDER BY co.compound_id
)

SELECT * FROM cte_comments
