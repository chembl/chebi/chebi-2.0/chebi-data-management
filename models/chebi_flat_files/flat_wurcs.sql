{{ config(alias='wurcs') }}
WITH cte_wurcs AS (
    SELECT
        w.structure_id,
        w.wurcs
    FROM {{ ref('wurcs') }} AS w
    INNER JOIN {{ ref('flat_structures') }} AS s
        ON w.structure_id = s.id
)

SELECT * FROM cte_wurcs;
