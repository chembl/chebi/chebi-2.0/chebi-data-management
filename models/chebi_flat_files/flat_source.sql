{{ config(alias='source') }}
WITH cte_source AS (
    SELECT
        so.id,
        so.name,
        so.url,
        so.prefix,
        so.description
    FROM {{ ref('source') }} AS so
)

SELECT * FROM cte_source
