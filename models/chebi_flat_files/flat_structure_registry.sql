{{ config(alias='structure_registry') }}

WITH structure_registry AS (
    SELECT
        sr.id,
        sr.structure_id,
        sr.layers,
        sr.hash
    FROM {{ ref('structure_registry') }} AS sr
)

SELECT * FROM structure_registry
