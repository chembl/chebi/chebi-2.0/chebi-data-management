{{ config(alias='status') }}
WITH cte_status AS (
    SELECT
        s.id,
        s.name
    FROM {{ ref('status') }} AS s
)

SELECT * FROM cte_status
