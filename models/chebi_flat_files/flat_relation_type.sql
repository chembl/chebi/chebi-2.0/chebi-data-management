{{ config(alias='relation_type') }}
WITH cte_relation_type AS (
    SELECT
        rt.id,
        rt.code,
        rt.allow_cycles,
        rt.description
    FROM {{ ref('relation_type') }} AS rt
)

SELECT * FROM cte_relation_type
