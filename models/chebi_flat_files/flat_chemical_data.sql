{{ config(alias='chemical_data') }}
WITH cte_chemical_data AS (
    SELECT
        cd.id,
        vcd.compound_id,
        vs.structure_id,
        cd.status_id,
        vcd.formula,
        vcd.charge,
        vcd.mass,
        vcd.monoisotopic_mass
    FROM {{ ref('flat_compounds') }} AS fc
    INNER JOIN {{ ref('chemical_data') }} AS cd
        ON fc.id = cd.compound_id
    INNER JOIN {{ ref('valid_chemical_data') }} AS vcd
        ON cd.compound_id = vcd.compound_id
    INNER JOIN {{ ref('valid_structures') }} AS vs
        ON cd.compound_id = vs.compound_id
)

SELECT * FROM cte_chemical_data
