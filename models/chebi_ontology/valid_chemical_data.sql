-- Some compounds have the chemical_data in one of their children, so we need to explore
-- the secondary ids to validate this and get the chemical_data information from there. This makes sense because the default structure
-- is not always in the primary id, so the chemical_data information is linked to the compound that has the default_structure
WITH cte_chemical_data_parents AS (
    SELECT DISTINCT
        c.id AS compound_id,
        --c2.id AS child_id,
        cd.formula,
        cd.charge::text,
        cd.mass::text,
        cd.monoisotopic_mass::text
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('chemical_data') }} AS cd ON c.id = cd.compound_id
    WHERE
        c.parent_id IS NULL
        --AND c.id IN (41981, 27427)
),

cte_chemical_data_children AS (
    SELECT
        c.id AS compound_id,
        --c2.id AS child_id,
        STRING_AGG(DISTINCT cd.formula, ';') AS formula,
        STRING_AGG(DISTINCT cd.charge::text, ';') AS charge,
        STRING_AGG(DISTINCT cd.mass::text, ';') AS mass,
        STRING_AGG(DISTINCT cd.monoisotopic_mass::text, ';') AS monoisotopic_mass
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
        LEFT JOIN {{ ref('chemical_data') }} AS cd
            ON c2.id = cd.compound_id AND cd.structure_id IS NOT NULL
    GROUP BY c.id
),

cte_chemical_data AS (
    SELECT
        pt.compound_id,
        COALESCE(pt.formula, ch.formula) AS formula,
        COALESCE(pt.charge, ch.charge) AS charge,
        COALESCE(pt.mass, ch.mass) AS mass,
        COALESCE(pt.monoisotopic_mass, ch.monoisotopic_mass) AS monoisotopic_mass
    FROM cte_chemical_data_parents AS pt
        LEFT JOIN cte_chemical_data_children AS ch ON pt.compound_id = ch.compound_id
)

SELECT
    ROW_NUMBER() OVER (ORDER BY c.compound_id) AS id,
    c.*
FROM cte_chemical_data AS c
