-- These tables are used in the other queries as a base to get the information about synonyms and database references
-- as well as the related axioms.

-- Here we are creating a CTE to get the exact synonyms of the compounds as well as its axioms.
-- The exact synonyms of a comppund are all the names wit type = 'IUPAC NAME'. Additionally, we need to check if the
-- secondary_id's names have some variant of the IUPAC NAME gotten for the current compound (the same IUPAC NAME uppercased,
-- or capitalized, and include them as well). That's what we do in  THE LOWER(n.ascii_name) IN (...) condition.
--
-- About axioms:
-- In the chebi.owl file, it is necessary to mark two specific axioms for the exact synonyms: the source of the name (column source.name) and
-- the name type (column name.type), that's what we do in the las two columns (axioms_exact_synonym and axioms_exact_synonym_type)

WITH cte_exact_synonym_disperse AS (
    SELECT
        n.compound_id,
        CONCAT('CHEBI:', n.compound_id) AS curie,
        n.ascii_name,
        n.name,
        s.name AS axiom,
        CONCAT('chebi', ':', REPLACE(n."type", ' ', '_')) AS axiom_type -- convert IUPAC NAME TO IUPAC_NAME
    FROM {{ ref('compounds') }} AS c
        INNER JOIN {{ ref('names') }} AS n ON c.id = n.compound_id
        INNER JOIN {{ ref('source') }} AS s ON n.source_id = s.id
    WHERE
        n."type" = 'IUPAC NAME'
        AND c.parent_id IS NULL
    UNION
    SELECT
        c.id AS compound_id,
        c.chebi_accession AS curie,
        n.ascii_name,
        n.name,
        REPLACE(s.name, ' ', '_') AS axiom,
        CASE
            WHEN n."type" = 'IUPAC NAME' THEN CONCAT('chebi', ':', REPLACE(n."type", ' ', '_'))
        END AS axiom_type -- convert IUPAC NAME TO IUPAC_NAME
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
        INNER JOIN {{ ref('names') }} AS n ON c2.id = n.compound_id
        INNER JOIN {{ ref('source') }} AS s ON n.source_id = s.id
    WHERE
        LOWER(n.ascii_name) IN (SELECT LOWER(n.ascii_name) FROM {{ ref('names') }} AS n WHERE n."type" = 'IUPAC NAME')
        AND c.parent_id IS NULL
),

cte_exact_synonyms AS (
    SELECT
        ROW_NUMBER() OVER (ORDER BY esd.compound_id) AS id,
        esd.compound_id,
        esd.curie,
        'owl:Class' AS property_type,
        esd.ascii_name,
        esd.name AS exact_synonym,
        esd.axiom,
        esd.axiom_type
    FROM cte_exact_synonym_disperse AS esd
        INNER JOIN {{ ref('relations_by_compound') }} AS vr ON esd.compound_id = vr.compound_id
)

SELECT * FROM cte_exact_synonyms
