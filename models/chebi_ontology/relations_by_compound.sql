WITH cte_relations_by_compounds_disperse AS (
    SELECT
        r.compound_id,
        --rt.code  AS relation_name,
        CASE
            WHEN r.relation_type_id = 1 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_functional_parent,
        CASE
            WHEN r.relation_type_id = 2 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_parent_hydride,
        CASE
            WHEN r.relation_type_id = 3 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_has_part,
        CASE
            WHEN r.relation_type_id = 4 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_has_role,
        CASE
            WHEN r.relation_type_id = 5 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_parents,
        CASE
            WHEN r.relation_type_id = 6 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_conjugate_acid_of,
        CASE
            WHEN r.relation_type_id = 7 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_conjugate_base_of,
        CASE
            WHEN r.relation_type_id = 8 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_enantiomers_of,
        CASE
            WHEN r.relation_type_id = 9 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_is_part_of,
        CASE
            WHEN r.relation_type_id = 10 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_is_substituent_group_from,
        CASE
            WHEN r.relation_type_id = 11 THEN STRING_AGG('CHEBI:' || r.final_id, '|')
        END AS compounds_is_tautomer_of
    FROM {{ ref('valid_relations') }} AS r
        -- left join to take into account the special ones chebi ids: 24431, 50906, 36342,
        LEFT JOIN {{ ref('relation_type') }} AS rt ON r.relation_type_id = rt.id
    GROUP BY
        r.compound_id,
        r.relation_type_id
),

cte_relations_by_compound AS (
    SELECT
        crd.compound_id,
        (ARRAY_AGG(compounds_functional_parent) FILTER (WHERE compounds_functional_parent IS NOT NULL))[1] AS compounds_functional_parent,
        (ARRAY_AGG(compounds_parent_hydride) FILTER (WHERE compounds_parent_hydride IS NOT NULL))[1] AS compounds_parent_hydride,
        (ARRAY_AGG(compounds_has_part) FILTER (WHERE compounds_has_part IS NOT NULL))[1] AS compounds_has_part,
        (ARRAY_AGG(compounds_has_role) FILTER (WHERE compounds_has_role IS NOT NULL))[1] AS compounds_has_role,
        (ARRAY_AGG(compounds_parents) FILTER (WHERE compounds_parents IS NOT NULL))[1] AS compounds_parents,
        (ARRAY_AGG(compounds_conjugate_acid_of) FILTER (WHERE compounds_conjugate_acid_of IS NOT NULL))[1] AS compounds_conjugate_acid_of,
        (ARRAY_AGG(compounds_conjugate_base_of) FILTER (WHERE compounds_conjugate_base_of IS NOT NULL))[1] AS compounds_conjugate_base_of,
        (ARRAY_AGG(compounds_enantiomers_of) FILTER (WHERE compounds_enantiomers_of IS NOT NULL))[1] AS compounds_enantiomers_of,
        (ARRAY_AGG(compounds_is_part_of) FILTER (WHERE compounds_is_part_of IS NOT NULL))[1] AS compounds_is_part_of,
        (ARRAY_AGG(compounds_is_substituent_group_from) FILTER (WHERE compounds_is_substituent_group_from IS NOT NULL))[1] AS compounds_is_substituent_group_from,
        (ARRAY_AGG(compounds_is_tautomer_of) FILTER (WHERE compounds_is_tautomer_of IS NOT NULL))[1] AS compounds_is_tautomer_of
    FROM cte_relations_by_compounds_disperse AS crd
    GROUP BY crd.compound_id
)

SELECT
    ROW_NUMBER() OVER (ORDER BY ccbr.compound_id) AS id,
    ccbr.*
FROM cte_relations_by_compound AS ccbr
