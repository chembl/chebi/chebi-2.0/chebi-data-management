WITH cte_compound_origins_parents AS (
    SELECT
        co.id AS compound_origin_id,
        c.id AS compound_id,
        co.species_source_id,
        co.species_text,
        co.species_accession,
        co.component_source_id,
        co.component_text,
        co.component_accession,
        co.strain_text,
        co.source_id,
        co.source_accession,
        co.comments,
        co.status_id
    FROM {{ ref('compounds') }} AS c
        INNER JOIN {{ ref('compound_origins') }} AS co ON c.id = co.compound_id
    WHERE c.parent_id IS NULL
),

cte_compound_origins_children AS (
    SELECT
        co.id AS compound_origin_id,
        c.id AS compound_id,
        co.species_source_id,
        co.species_text,
        co.species_accession,
        co.component_source_id,
        co.component_text,
        co.component_accession,
        co.strain_text,
        co.source_id,
        co.source_accession,
        co.comments,
        co.status_id
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
        INNER JOIN {{ ref('compound_origins') }} AS co ON c2.id = co.compound_id
),

cte_compound_origins AS (
    SELECT pt.* FROM cte_compound_origins_parents AS pt
    UNION
    SELECT ch.* FROM cte_compound_origins_children AS ch
)

SELECT
    ROW_NUMBER() OVER (ORDER BY cco.compound_origin_id) AS id,
    cco.*
FROM cte_compound_origins AS cco
