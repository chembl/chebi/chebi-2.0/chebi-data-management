-- (CHEBI:15377) -> relation -> (ALL NODES)
-- Here we are getting all the relation and (ALL NODES) for the compound: 'has_role', 'has_functional_parent', 'is_tautomer_of' etc.
-- Special NOTE: In the first part of the UNION, we get the first set of relations by the current compound, but it is necessary
-- (additionally) to take into account the relations of the secondary ids, that's what we do in the second part of the UNION.
-- For example: CHEBI:15570 does not get information about its parents in the first part of the union, but we can get the parents
-- in the second part of the union.
--
WITH cte_compounds_relations AS (
    SELECT
        c.id AS compound_id, -- these are the primary ids
        r.final_id,
        r.relation_type_id
    FROM {{ ref('compounds') }} AS c
        INNER JOIN {{ ref('relation') }} AS r ON c.id = r.init_id
        INNER JOIN {{ ref('relation_type') }} AS rt ON r.relation_type_id = rt.id
    WHERE
        r.final_id <> 27189 -- We're NOT showing compounds that have 'unclassified' AS a parent, the chebi_id FOR unclassified IS 27189
        AND c.parent_id IS NULL
    UNION
    SELECT
        c.id AS compound_id, -- these are the secondary ids
        --c2.id AS alternative_id,
        r.final_id,
        r.relation_type_id
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
        INNER JOIN {{ ref('relation') }} AS r ON c2.id = r.init_id
    WHERE
        r.final_id <> 27189
    UNION
    SELECT
        c.id AS compound_id,
        NULL AS init_id,
        NULL AS relation_type_id
    FROM {{ ref('compounds') }} AS c
    -- There ARE 3 chebi ids that are special, they don't have any parents because they are the upper definitions:
    -- chemical entity, role and subatomic particle. Anyway, they must appear in the chebi ontology!
    WHERE c.id IN (24431, 50906, 36342)
),

-- This cte is necessary because we need to get the primary ids of the final (Nodes): CHEBI:15377 -> has_role -> (CHEBI:75770) is not
-- correct because, in this case, the final node CHEBI:75770 is a secondary id of CHEBI:77746. The correct relation would be then, using the
-- primary id: CHEBI:15377 -> has_role -> CHEBI:77746
cte_final_primary_compounds AS (
    SELECT
        cr.compound_id,
        cr.final_id,
        cr.relation_type_id
    FROM cte_compounds_relations AS cr -- the previous cte_compounds_relations
        LEFT JOIN {{ ref('compounds') }} AS c ON cr.final_id = c.id
    WHERE c.parent_id IS NULL
    UNION
    SELECT
        cr.compound_id,
        c.parent_id AS final_id,
        cr.relation_type_id
    FROM cte_compounds_relations AS cr
        LEFT JOIN {{ ref('compounds') }} AS c ON cr.final_id = c.id
    WHERE c.parent_id IS NOT NULL
)

SELECT
    ROW_NUMBER() OVER (ORDER BY cpc.compound_id) AS id,
    cpc.*
FROM cte_final_primary_compounds AS cpc
