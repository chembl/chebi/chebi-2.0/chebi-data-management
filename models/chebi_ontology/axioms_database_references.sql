WITH cte_database_references AS (
    SELECT
        ROW_NUMBER() OVER (ORDER BY drd.compound_id) AS id,
        drd.compound_id,
        drd.curie,
        'owl:Class' AS property_type,
        drd.prefix AS axiom,
        drd.type,
        CONCAT(drd.prefix, ':', drd.accession_number) AS database_references
    FROM {{ ref('valid_database_references') }} AS drd
        INNER JOIN {{ ref('relations_by_compound') }} AS vr ON drd.compound_id = vr.compound_id
    ORDER BY database_references
)

SELECT * FROM cte_database_references
