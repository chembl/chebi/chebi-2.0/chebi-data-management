-- The alternative IDS are all the compounds ids which are children of the current compound.
WITH cte_alternative_ids AS (
    SELECT
        vsi.id, --parent_id
        STRING_AGG(vsi.chebi_accession, '|') AS alternative_ids --children ids
    FROM {{ ref('valid_secondary_ids') }} AS vsi
    GROUP BY vsi.id
),

cte_database_references AS (
    SELECT
        drd.compound_id,
        STRING_AGG(DISTINCT CONCAT(drd.prefix, ':', drd.accession_number), '|') AS database_references
    FROM {{ ref('valid_database_references') }} AS drd
    GROUP BY drd.compound_id
),

cte_exact_synonym AS (
    SELECT
        esd.compound_id,
        STRING_AGG(esd.exact_synonym, '|') AS exact_synonym
    FROM {{ ref('axioms_exact_synonyms') }} AS esd
    GROUP BY esd.compound_id
),

cte_related_synonym AS (
    SELECT
        rsd.compound_id,
        STRING_AGG(rsd.related_synonym, '|') AS related_synonym
    FROM {{ ref('axioms_related_synonyms') }} AS rsd
    GROUP BY rsd.compound_id
),

cte_wurcs AS (
    SELECT
        w.wurcs,
        s.compound_id
    FROM {{ ref('valid_structures') }} AS s
        INNER JOIN {{ source('chebi_production', 'wurcs') }} AS w ON s.id = w.structure_id
)

SELECT DISTINCT
    c.id,
    c.chebi_accession AS curie,
    c.modified_on,
    'owl:Class' AS property_type,
    c.name AS label,
    c.ascii_name,
    cr.compounds_functional_parent,
    cr.compounds_parent_hydride,
    cr.compounds_has_part,
    cr.compounds_has_role,
    cr.compounds_parents,
    cr.compounds_conjugate_acid_of,
    cr.compounds_conjugate_base_of,
    cr.compounds_enantiomers_of,
    --cr.compounds_is_part_of,
    cr.compounds_is_substituent_group_from,
    cr.compounds_is_tautomer_of,
    c.definition AS description,
    cd.charge,
    cd.formula,
    s.standard_inchi AS inchi,
    s.standard_inchi_key AS inchikey,
    w.wurcs,
    cd.mass,
    cd.monoisotopic_mass,
    s.smiles,
    aids.alternative_ids,
    c.chebi_accession AS chebi_id,
    'chebi_ontology' AS namespace,
    dr.database_references,
    es.exact_synonym,
    rs.related_synonym,
    c.stars AS star_id,
    'chebi:' || c.stars || '_STAR' AS stars
FROM {{ ref('compounds') }} AS c
    INNER JOIN {{ ref('relations_by_compound') }} AS cr ON c.id = cr.compound_id
    LEFT JOIN {{ ref('valid_chemical_data') }} AS cd ON c.id = cd.compound_id
    LEFT JOIN {{ ref('valid_structures') }} AS s ON c.id = s.compound_id
    LEFT JOIN cte_wurcs AS w ON c.id = w.compound_id
    LEFT JOIN cte_alternative_ids AS aids ON c.id = aids.id
    LEFT JOIN cte_database_references AS dr ON c.id = dr.compound_id
    LEFT JOIN cte_exact_synonym AS es ON c.id = es.compound_id
    LEFT JOIN cte_related_synonym AS rs ON c.id = rs.compound_id
WHERE
    c.parent_id IS NULL
ORDER BY c.id
