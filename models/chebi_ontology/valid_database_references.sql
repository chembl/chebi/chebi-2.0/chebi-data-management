-- The database reference information is gotten from the current compound and all its secondary ids.
-- database_references = database_references_current_compound and database_references_secondaries.
-- We discover in the old java repository that the 'accession' string was deleted in database_accession_name column,
-- It is necessary to do the same here to get the correct axioms.
WITH cte_database_references_disperse AS (
    SELECT
        da.compound_id,
        CONCAT('CHEBI:', da.compound_id) AS curie,
        REPLACE(TRIM(da.accession_number), ' ', '_') AS accession_number,
        da.type,
        CASE
            WHEN da.type = 'CAS' THEN 'cas'
            ELSE sr.prefix
        END AS prefix,
        sr.name AS source_name,
        da.source_id
    FROM {{ ref('compounds') }} AS c
        INNER JOIN {{ ref('database_accession') }} AS da ON c.id = da.compound_id
        INNER JOIN {{ ref('source') }} AS sr ON da.source_id = sr.id
    WHERE
        c.parent_id IS NULL
    UNION
    SELECT
        c.id AS compound_id,
        c.chebi_accession AS curie,
        REPLACE(TRIM(da.accession_number), ' ', '_') AS accession_number,
        da.type,
        CASE
            WHEN da.type = 'CAS' THEN 'cas'
            ELSE sr.prefix
        END AS prefix,
        sr.name AS source_name,
        da.source_id
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
        INNER JOIN {{ ref('database_accession') }} AS da ON c2.id = da.compound_id
        INNER JOIN {{ ref('source') }} AS sr ON da.source_id = sr.id
    WHERE
        c.parent_id IS NULL
)

SELECT
    ROW_NUMBER() OVER (ORDER BY cdrd.compound_id) AS id,
    cdrd.*
FROM cte_database_references_disperse AS cdrd
