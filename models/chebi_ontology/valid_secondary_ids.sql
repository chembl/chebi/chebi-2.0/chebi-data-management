WITH valid_secondary_ids AS (
    SELECT
        c.id, -- primary id
        c2.id AS secondary_id,
        c2.chebi_accession
    FROM {{ ref('compounds') }} AS c
        INNER JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
    ORDER BY c.id
)

SELECT * FROM valid_secondary_ids
