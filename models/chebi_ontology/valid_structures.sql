-- Some compounds have the default_structure in one of their secondary ids, so we need to explore
-- them to validate this and get the structure information from there
WITH cte_structures_parents AS (
    SELECT DISTINCT
        s.id AS structure_id,
        c.id AS compound_id,
        s.standard_inchi_key,
        s.standard_inchi,
        s.smiles
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('structures') }} AS s ON c.id = s.compound_id
    WHERE
        s.molfile IS NOT NULL
        AND s.default_structure
        AND c.parent_id IS NULL
),

cte_structures_children AS (
    SELECT DISTINCT
        s.id AS structure_id,
        c.id AS compound_id,
        s.standard_inchi_key,
        s.standard_inchi,
        s.smiles
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
        LEFT JOIN {{ ref('structures') }} AS s ON c2.id = s.compound_id
    WHERE
        s.molfile IS NOT NULL
        AND s.default_structure
),

cte_structure AS (
    SELECT pt.* FROM cte_structures_parents AS pt
    UNION
    SELECT ch.* FROM cte_structures_children AS ch
    WHERE ch.compound_id NOT IN (SELECT compound_id FROM cte_structures_parents)
)

SELECT
    ROW_NUMBER() OVER (ORDER BY cvs.compound_id) AS id,
    cvs.*
FROM cte_structure AS cvs
