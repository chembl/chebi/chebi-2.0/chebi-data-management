-- Here we are creating a CTE to get the related synonyms of the compounds as well as its axioms.
-- The related_synonym information is gotten from the current compound (primary id) and all its secondary ids.
-- related_synonym = related_synonym_current_compound and related_synonym_secondaries.

-- About axioms:
-- In the chebi.owl file, it is necessary to mark the same two specific axioms for the related synonyms
-- in the same way we did it for the exact synonyms: the source of the name (column source.name) and
-- the name type (column name.type), that's what we do in the last two columns (axioms_related_synonym and
-- axioms_related_synonym_type)

WITH cte_related_synonym_disperse AS (
    SELECT
        n.compound_id,
        CONCAT('CHEBI:', n.compound_id) AS curie,
        n.ascii_name,
        n.name,
        s.prefix AS axiom,
        CASE
            WHEN n."type" = 'IUPAC NAME' THEN CONCAT('chebi', ':', REPLACE(n."type", ' ', '_'))
            WHEN n."type" = 'BRAND NAME' THEN CONCAT('chebi', ':', REPLACE(n."type", ' ', '_'))
            WHEN n."type" = 'INN' THEN CONCAT('chebi', ':', n."type")
        END AS axiom_type
    FROM {{ ref('compounds') }} AS c
        INNER JOIN {{ ref('names') }} AS n ON c.id = n.compound_id
        INNER JOIN {{ ref('source') }} AS s ON n.source_id = s.id
        LEFT JOIN {{ ref('axioms_exact_synonyms') }} AS es ON n.ascii_name = es.ascii_name
    WHERE
        es.ascii_name IS NULL
        AND c.parent_id IS NULL
    UNION
    SELECT
        c.id AS compound_id,
        c.chebi_accession AS curie,
        n.ascii_name,
        n.name,
        s.prefix AS axiom,
        CASE
            WHEN n."type" = 'IUPAC NAME' THEN CONCAT('chebi', ':', REPLACE(n."type", ' ', '_'))
            WHEN n."type" = 'BRAND NAME' THEN CONCAT('chebi', ':', REPLACE(n."type", ' ', '_'))
            WHEN n."type" = 'INN' THEN CONCAT('chebi', ':', n."type")
        END AS axiom_type
    FROM {{ ref('compounds') }} AS c
        LEFT JOIN {{ ref('compounds') }} AS c2 ON c.id = c2.parent_id
        INNER JOIN {{ ref('names') }} AS n ON c2.id = n.compound_id
        INNER JOIN {{ ref('source') }} AS s ON n.source_id = s.id
        LEFT JOIN {{ ref('axioms_exact_synonyms') }} AS es ON n.ascii_name = es.ascii_name
    WHERE
        es.ascii_name IS NULL
        AND c.parent_id IS NULL
),

cte_related_synonyms AS (
    SELECT
        ROW_NUMBER() OVER (ORDER BY rsd.compound_id) AS id,
        rsd.compound_id,
        rsd.curie,
        'owl:Class' AS property_type,
        rsd.ascii_name,
        rsd.name AS related_synonym,
        rsd.axiom,
        rsd.axiom_type
    FROM cte_related_synonym_disperse AS rsd
        INNER JOIN {{ ref('relations_by_compound') }} AS vr ON rsd.compound_id = vr.compound_id
)

SELECT * FROM cte_related_synonyms
