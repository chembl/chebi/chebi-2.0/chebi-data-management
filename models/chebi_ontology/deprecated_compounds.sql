-- 1. Deprecated because of a merging process. Basically, when a compound c0 is merged with a compound C1, the field parent_id
-- in the c0 compound is populated with the id of C1.
WITH cte_deprecated_for_merging AS (
    SELECT
        c.id AS compound_id,
        c.chebi_accession AS curie,
        c.stars,
        c.status_id,
        'owl:Class' AS property_type,
        1 AS is_deprecated,
        CASE
            WHEN c.id NOT IN (27189) THEN CONCAT('CHEBI:', c.parent_id)
        END AS terms_merged_with,
        CASE
            -- This is a curie to describe the merged process: https://ontobee.org/ontology/IAO?iri=http://purl.obolibrary.org/obo/IAO_0000227
            WHEN c.id NOT IN (27189) THEN 'obo:IAO_0000227'
        END AS terms_merged_definition,
        CASE
            WHEN c.id NOT IN (27189) THEN NULL
            ELSE 'chebi_ontology'
        END AS namespace,
        CASE
            WHEN c.id NOT IN (27189) THEN NULL
            ELSE c.chebi_accession
        END AS object_property
    FROM {{ ref('compounds') }} AS c
    WHERE
        c.parent_id IS NOT NULL
        OR c.id = 27189 -- We must include the 'unclassified' definition, CHEBI:27189
)

SELECT
    ROW_NUMBER() OVER (ORDER BY cdc.compound_id) AS id,
    cdc.*
FROM cte_deprecated_for_merging AS cdc;
