from dbt.cli.main import dbtRunner, dbtRunnerResult
from pathlib import Path

def create_documentation():
    dbt = dbtRunner()
    current_folder = Path(__file__).parent
    # run the command
    deps_command: dbtRunnerResult = dbt.invoke(["deps"])
    if deps_command.success:
        dbt.invoke(["docs", "generate", "--target", "stg-postgres"])

        with open(current_folder / "target" / "index.html", mode="r") as index_html:
            index_html = index_html.read()

        index_html = index_html.replace('<div ng-show="tree.sources.length > 0">', '<div ng-show="false"')
        index_html = index_html.replace('<div class="switch">', '<div class="switch" hidden>')
        index_html = index_html.replace('<title>dbt Docs</title>', '<title>ChEBI Database Docs</title>')
        index_html = index_html.replace('<meta property="og:site_name" content="dbt Docs" />', '<meta property="og:site_name" content="ChEBI Database Docs" />')
        index_html = index_html.replace('<meta property="og:title" content="dbt Docs" />', '<meta property="og:title" content="ChEBI Database Docs" />')
        index_html = index_html.replace('<meta property="og:description" content="documentation for dbt" />', '<meta property="og:description" content="documentation for ChEBI" />')
        index_html = index_html.replace('<meta name="twitter:title" content="dbt Docs"/>', '<meta name="twitter:title" content="ChEBI Database Docs"/>')
        index_html = index_html.replace('<meta name="twitter:description" content="documentation for dbt"/>', '<meta name="twitter:description" content="documentation for ChEBI"/>')

        open(current_folder / "target" / "index.html", mode="w+").write(index_html)


if __name__ == '__main__':
    create_documentation()